module.exports = function (gulp, plugins, getFolders, mobileSettings) {
  function onError(e) {
    console.log(e.toString());
    this.emit('end');
  }

  /* Paths */
  let src = {
      main: 'src/scss/[^_]*.scss',
      components: 'src/components'
    },
    dist = {
      main: 'dist/css',
      mainUncompressed: 'dist/css/uncompressed',
      components: 'dist/css/components',
      componentsUncompressed: 'dist/css/components/uncompressed'
    },
    prefix = ['> 0.5%', 'last 2 versions', 'Firefox ESR', 'ie > 10', 'iOS > 9', 'not dead'];
  /* End Paths */

  return function () {

    gulp.src(src.main)
      .pipe(plugins.plumber({errorHandler: onError}))
      .pipe(plugins.if(!mobileSettings, plugins.sourcemaps.init()))
      .pipe(plugins.sass())
      .pipe(plugins.autoprefixer({browsers: prefix}))
      .pipe(plugins.if(mobileSettings, plugins.combineMq({beautify: false})))
      .pipe(plugins.changedInPlace({firstPass: true}))
      .pipe(gulp.dest(dist.mainUncompressed))
      .pipe(plugins.csso())
      .pipe(plugins.if(!mobileSettings, plugins.sourcemaps.write('./maps')))
      //.pipe(plugins.debug({title: 'CSS:'}))
      .pipe(gulp.dest(dist.main));

    let components = getFolders(src.components).map(function (folder) {
      return gulp.src(plugins.path.join(src.components, folder, '/*.scss'))
        .pipe(plugins.plumber({errorHandler: onError}))
        .pipe(plugins.concat(folder + '.scss'))
        .pipe(plugins.if(!mobileSettings, plugins.sourcemaps.init()))
        .pipe(plugins.sass())
        .pipe(plugins.autoprefixer({browsers: prefix}))
        .pipe(plugins.if(mobileSettings, plugins.combineMq({beautify: false})))
        .pipe(plugins.changedInPlace({firstPass: true}))
        .pipe(gulp.dest(dist.componentsUncompressed))
        .pipe(plugins.csso())
        .pipe(plugins.if(!mobileSettings, plugins.sourcemaps.write('./maps')))
        //.pipe(plugins.debug({title: 'CSS:'}))
        .pipe(gulp.dest(dist.components));
    });

    return plugins.merge(components);

  }
};