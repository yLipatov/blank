module.exports = function (gulp, plugins) {
  let vendorDir = './node_modules/',
    vendorArr = {
      bootstrap: {
        CSS: [
          'bootstrap/dist/css/bootstrap.min.css',
          'bootstrap-v4-rtl/dist/css/bootstrap-rtl.min.css'
        ],
        JS: 'bootstrap/dist/js/bootstrap.min.js'
      },
      jquery: {
        JS: 'jquery/dist/jquery.min.js'
      },
      jqueryBreakpoints: {
        JS: 'jquery-breakpoints/jquery.breakpoints.min.js'
      },
      lazysizes: {
        JS: [
          'lazysizes/lazysizes.js',
          'lazysizes/plugins/progressive/ls.progressive.js',
          'lazysizes/plugins/aspectratio/ls.aspectratio.js'
        ]
      },
      swiper: {
        CSS: 'swiper/dist/css/swiper.min.css',
        JS: 'swiper/dist/js/swiper.min.js'
      },
      fancybox: {
        CSS: '@fancyapps/fancybox/dist/jquery.fancybox.min.css',
        JS: '@fancyapps/fancybox/dist/jquery.fancybox.min.js'
      },
      wow: {
        JS: 'wow.js/dist/wow.min.js'
      },
      animate: {
        CSS: 'animate.css/animate.min.css'
      }
    },
    arr = [],
    i = 0;

  for (let prop in vendorArr) {
    arr[i] = '' + prop;
    i++;
  }

  // Vendor CSS
  let css = arr.map(function (arr) {
    let arrCSS = [];
    if(typeof vendorArr[arr].CSS === 'object') {
      vendorArr[arr].CSS.map(function (item, i) {
        arrCSS[i] = vendorDir + item;
      })
    } else {
      arrCSS[0] = vendorDir + vendorArr[arr].CSS;
    }

    return gulp.src(arrCSS ? arrCSS : [])
      .pipe(gulp.dest('dist/vendor/' + arr + '/css'));
  });

  // Vendor JS
  let js = arr.map(function (arr) {
    let arrJS = [];
    if(typeof vendorArr[arr].JS === 'object') {
      vendorArr[arr].JS.map(function (item, i) {
        arrJS[i] = vendorDir + item;
      })
    } else {
      arrJS[0] = vendorDir + vendorArr[arr].JS;
    }

    let min = arr === 'lazysizes';

    return gulp.src(arrJS ? arrJS : [])
      .pipe(plugins.if(min, plugins.concat(arr + '.min.js')))
      .pipe(plugins.if(min, plugins.uglify()))
      .pipe(gulp.dest('dist/vendor/' + arr + '/js'));
  });

  return function () {
    return plugins.merge(css, js);
  };
};